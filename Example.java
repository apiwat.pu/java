import java.util.Scanner;

public class Example {
    public static void main(String[] args) {
        Scanner inputdata = new Scanner(System.in);
        Scanner inputstring = new Scanner(System.in);
        int q = 1;
        while (q > 0) {
            System.out.print("Insert number of gard : ");
            int numberofgard = inputdata.nextInt();
            int temp;
            int n = 0, m = 1;
            if (numberofgard >= 2 && numberofgard <= 1000) {
                System.out.print("Enter the number of each card [ please space between each number ] : ");
                String digit = inputstring.nextLine();
                int checklength = (numberofgard * 2) - 1;
                int digitlength = digit.length();
                int digitloop = (digitlength + 1) / 2;

                if (digitlength == checklength) {
                    String[] digit4split = digit.split(" ");
                    int[] numbers = new int[digitlength];
                    for (int x = 0; x < digitloop; x++) {
                        numbers[x] = Integer.parseInt(digit4split[x]);
                    }
                    while (n < digitloop) {
                        for (int i = m; i < digitloop; i++) {
                            if (numbers[n] > numbers[i]) {
                                temp = numbers[i];
                                numbers[i] = numbers[n];
                                numbers[n] = temp;
                            }

                        }
                        n = n + 1;
                        m = m + 1;
                    }
                    for (int z = 0; z < digitloop; z++) {
                        if (numbers[z] > 0) {
                            int tem = numbers[z];
                            numbers[z] = numbers[0];
                            numbers[0] = tem;
                            break;
                        }
                    }
                    System.out.println("");
                    System.out.println("-------- RESULT --------");
                    System.out.println(numberofgard);
                    for (int x = 0; x < digitloop; x++) {
                        System.out.print(numbers[x]);
                    }
                    q = q + 1;
                    break;
                } else {
                    System.out.println("!!! Please You check you number !!! ");
                    continue;
                }
            } else {
                System.out.println("!!! You can insert 2 - 1000 digit !!!");
                continue;
            }
        }

    }
}
